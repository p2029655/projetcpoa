create table Utilisateur (
id NUMBER(6) primary key ,
email VARCHAR2(100),
password  VARCHAR2(20),
lastname VARCHAR2(30),
adress VARCHAR2(50),
zipcode number(6),
city VARCHAR(20),
role VARCHAR(20),
createdAt VARCHAR2(10)

)
create table categorie (
idTable NUMBER(6) primary key ,
nomCat VARCHAR2(20)
)
create table Produit (
idProd NUMBER(6) primary key ,
categorie VARCHAR2(20),
prix number (3,2),
description VARCHAR2(1000),
avis VARCHAR2(1000),
solde VARCHAR(20),
prixSolde number(3,4),
seuilAlerte number(4),
qtiteActu number(3)

)
alter table Produit
add constraint idTable_fk foreign key(idTable) references categorie;